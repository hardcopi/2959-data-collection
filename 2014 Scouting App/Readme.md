# CW Tech Data Application
The following is a list of notes regarding this application.

## Libraries Used

* Kendo Mobile (Telerik)
* Apache Cordova 3.20
* Jaydata (mostly utilized just for the database creation, phasing this out)
* JQuery

There is a limited amount of documentation so far in this program but this will be rectified over the off season period...
