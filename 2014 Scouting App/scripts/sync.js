

setTimeout(function() {
    $("#syncRobots").bind( "touchend", function(e) {
        console.log('Starting Sync');
        var db = openDatabase('Team2959', '', 'First Wave Scouting', 2 * 1024 * 1024);
        db.transaction(function (tx) {
            var sql = 'SELECT * FROM Robots';
            tx.executeSql(sql, [], function (tx, results) {
                var len = results.rows.length, i;
                var serverIp = localStorage['serverIp'];
    			console.log('Number of Robots Found...'+len);
                for (i = 0; i < len; i++) {
    				var output = results.rows.item(i);
                    console.log(output);
                    $('#status').html('Syncing '+output.Name);
                    $.ajax({
                      type: "POST",
                      url: 'http://'+serverIp+'/syncAll.php?db=Robots&update=1',
                      data: output,
                      success: function(data) {
                      }
                    });
                            $('.progress').html('Updated '+output.Name);
                
               }
            });    
       }, function() {
           $('.progress').html('There was an error');
       }, function() {
        	console.log('updateRobotsDB');
            var serverIp = localStorage['serverIp'];
            var db = openDatabase('Team2959', '', 'First Wave Scouting', 2 * 1024 * 1024);

                       $.ajax({
                  type: "POST",
                  url: 'http://'+serverIp+'/syncAll.php?db=Robots',
                  success: function(data) {
                      console.log('Successly Retrieved Information....');
                      
                      data = JSON.parse(data);
                      var db = openDatabase('Team2959', '', 'First Wave Scouting', 2 * 1024 * 1024);
                      db.transaction(function (tx) {
                          console.log('Deleting old robots');
                            var sql = "DELETE FROM Robots";
                            tx.executeSql(sql);
                      });
                      setTimeout(function() {  
                        db.transaction(function(tx) {
                        var data_lines = data.length;
                        var key = 0;             
                              while (key != data_lines) {
                                  $('.progress').html('Adding '+key+': '+data[key]['Name']);
                                  tx.executeSql('INSERT INTO Robots (Name, Number, comment, scout, Drive_Train, Auton, Aim_Auton, Hot, Zone, Defender, Aim_High, Truss, Passes, Last_Modified, Picture, Robot_Type, Json, opr, events, dpr, oprrank, oprpercent) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',
                                            [ data[key]['Name'],        data[key]['Number'],        data[key]['comment'],   
                                              data[key]['scout'],       data[key]['Drive_Train'],   data[key]['Auton'],         
                                              data[key]['Aim_Auton'],   data[key]['Hot'],           data[key]['Zone'],          
                                              data[key]['Defender'],    data[key]['Aim_High'],      data[key]['Truss'],         
                                              data[key]['Passes'],      data[key]['Last_Modified'], data[key]['Picture'],       
                                              data[key]['Robot_Type'],  data[key]['Json'],          data[key]['opr'],           
                                              data[key]['events'],      data[key]['dpr'],           data[key]['oprrank'],   
                                              data[key]['oprpercent'] 
                                            ],
                                            function(tx) {
                                                console.log('Successly Added');
                                                var roboList = $('#roboListView').data('kendoMobileListView');
                                                roboList.dataSource.read();
                                                roboList.refresh();
                                            },
                                            function(transaction, e) {
                                                console.log('Error: '+e.message+' (Code '+e.code+')');
                                  });
                                  key++;
                           }  
                          });
                     }, 0);
                  }
                });
       });
    });
}, 300);

           

