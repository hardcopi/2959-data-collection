    var Category = $data.define("Category", {
        Name: String
    });

    var AppSettings = $data.define("AppSettings", {
        ShowNewItemInList: Boolean
    });

    var Match = $data.define("Match", {
        Match: Number,
        Cycle: String,
        allianceColor: String,
        comment: String,
        Venue: String,
        oneLight: String,
        twoLight: String,
        threeLight: String,
        Team1: String,
        Team2: String,
        Team3: String,
        team1Truss: String,
        team1Catch: String,
        team1HighScore: String,
        team1LowScore: String,
        team2Truss: String,
        team2Catch: String,
        team2HighScore: String,
        team2LowScore: String,
        team3Truss: String,
        team3Catch: String,
        team3HighScore: String,
        team3LowScore: String,
        
        team1High: String,
        team1Low: String,
        team2High: String,
        team2Low: String,
        team3High: String,
        team3Low: String
    });

document.addEventListener("deviceready", function() {
	$("#capturePhotoButton").bind("click", function() {
        navigator.camera.getPicture(onSuccess, onFail, {
            quality: 50,
            destinationType: Camera.DestinationType.DATA_URL
        }); 
    });
    
}, false);

function onSuccess(imageData) {
    $('#smallImage').attr('src', "data:image/jpeg;base64," + imageData);
    viewModel.set("Picture", imageData);
}

function onFail(message) {
    alert('Failed because: ' + message);
}

    

    