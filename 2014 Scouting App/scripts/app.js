robotsDB = new RobotsDB({name: 'webSql', databaseName: 'Team2959'});

var viewModel = {};
var matchesViewModel = {};   

robotsDB.onReady().then(function () {

    cycleArray  = new Array();  
    cycleChecks = new Array();
    cycleFails  = new Array();
    
    viewModel = kendo.observable({
        app: null,
        Robots: robotsDB.Robots.asKendoDataSource(), 
        Robot: null,
        RobotStatusFilter: 0,
        appSettings: null,
        
        // Create a new robot entry in the viewModel
        newRobot: function() {                    
            viewModel.set('Robot',viewModel.Robots.createItem());
            // Set the Picture to a standard 1x1 blank jpg
            viewModel.Robot.set('Picture', 'data:image/jpg;base64,/9j/4QAYRXhpZgAASUkqAAgAAAAAAAAAAAAAAP/sABFEdWNreQABAAQAAABQAAD/4QMtaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLwA8P3hwYWNrZXQgYmVnaW49Iu+7vyIgaWQ9Ilc1TTBNcENlaGlIenJlU3pOVGN6a2M5ZCI/PiA8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJBZG9iZSBYTVAgQ29yZSA1LjMtYzAxMSA2Ni4xNDU2NjEsIDIwMTIvMDIvMDYtMTQ6NTY6MjcgICAgICAgICI+IDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+IDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bXA6Q3JlYXRvclRvb2w9IkFkb2JlIFBob3Rvc2hvcCBDUzYgKE1hY2ludG9zaCkiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6NTFBOEE4MDMyMTk4MTFFMzhGMEVFNjE3MzU3QzFEQTUiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6NTFBOEE4MDQyMTk4MTFFMzhGMEVFNjE3MzU3QzFEQTUiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDo1MUE4QTgwMTIxOTgxMUUzOEYwRUU2MTczNTdDMURBNSIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDo1MUE4QTgwMjIxOTgxMUUzOEYwRUU2MTczNTdDMURBNSIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/Pv/uAA5BZG9iZQBkwAAAAAH/2wCEAAICAgICAgICAgIDAgICAwQDAgIDBAUEBAQEBAUGBQUFBQUFBgYHBwgHBwYJCQoKCQkMDAwMDAwMDAwMDAwMDAwBAwMDBQQFCQYGCQ0LCQsNDw4ODg4PDwwMDAwMDw8MDAwMDAwPDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDP/AABEIAAEAAQMBEQACEQEDEQH/xABKAAEAAAAAAAAAAAAAAAAAAAAJAQEAAAAAAAAAAAAAAAAAAAAAEAEAAAAAAAAAAAAAAAAAAAAAEQEAAAAAAAAAAAAAAAAAAAAA/9oADAMBAAIRAxEAPwB/Af/Z');
        },
        
        // Save the robot when you press the save button
        saveRobot: function() {
            // Setup the variables
            // Robot is the individual robot just entered
            // Robots are ALL the robots
            var Robot = viewModel.Robot, Robots = viewModel.Robots;
 			// ID smallImage is the robot image you took
            var picture = $('#smallImage').attr('src');
            // Add the image into the viewModel.Robot variable
            viewModel.Robot.set('Picture', picture);
			// Set the date you are modifying it
            viewModel.Robot.set('Last_Modified', new Date());
			// If the robot is brand new then you have to add it to Robots
            if (Robot.isNew()) {
                Robots.add(viewModel.Robot);
            }
            // Sync the viewModel with the database
            Robots.sync();
            // After Sync Navigate back to the previous page
            viewModel.app.navigate("#:back");
        },
        
        // When you select the robot it grabs all the data for the robot you touchended on
        // and places it inside the Robot variable, but first it empties the old info out
        selectRobot: function(e) {
            // Reset the robot when it is selected so anything left in the form will go away
            viewModel.set('Robot', undefined);
            viewModel.set('Robot', e.dataItem);
            
            var json = e.dataItem.Json;
            
            $('#blueAlliance').html(json);
            $('#json').kendoMobileListView({ style: 'inset' });
        },
        
        // Delete the robot
        removeRobot: function(e) {
            viewModel.Robots.remove(viewModel.Robot);
 			// After you perform a function that changes the viewModel you need to sync it together
            viewModel.Robots.sync();
        },
                
        // Sync Data with server
        syncData: function(e) {
            console.log('Starting Sync');
        	console.log(e);
            var Number = viewModel.Robot.get('Number');
            console.log(Number);

            var db = openDatabase('Team2959', '', 'First Wave Scouting', 2 * 1024 * 1024);
            db.transaction(function (tx) {
                var sql = 'SELECT * FROM Robots WHERE Number = \''+Number+'\'';
                tx.executeSql(sql, [], function (tx, results) {
                    var len = results.rows.length, i;
                    var serverIp = localStorage['serverIp'];
					console.log('Number of Robots Found...'+len);
                    for (i = 0; i < len; i++) {
        				var output = results.rows.item(i);
                        console.log(output);
                        $('#status').html('Syncing '+output.Name);
                        $.ajax({
                          type: "POST",
                          url: 'http://'+serverIp+'/syncAll.php?db=Robots&update=1',
                          data: output,
                          success: function(data) {
                              }
                        });
                                alert('Updated '+output.Name);
                    
                   }
                    
                });
                        
            });
		},
        
        updateRobotsDB: function() {
	console.log('updateRobotsDB');
    var serverIp = localStorage['serverIp'];
    var db = openDatabase('Team2959', '', 'First Wave Scouting', 2 * 1024 * 1024);

    $.ajax({
          type: "POST",
          url: 'http://'+serverIp+'/syncAll.php?db=Robots',
          success: function(data) {
              console.log('Success');
              //if (imported[data[key]['Number']] != 1) {
              console.log('Deleting old robots');
              data = JSON.parse(data);
                db.transaction(function (tx) {
                    var sql = "DELETE FROM Robots";
                    tx.executeSql(sql);
                });
              setTimeout(function() {
                  
              for (var key in data) {
                
                  console.log(data[key]['Name']);
                  //if (imported[data[key]['Number']] != 1) {
                        $('#status').html('Adding: '+data[key]['Number']);
	    				robotsDB.Robots.add(
                    		 { Name : data[key]['Name'], 
                             Number : data[key]['Number'],
                             comment : data[key]['comment'],
                             scout : data[key]['scout'],
                             Drive_Train : data[key]['Drive_Train'],
                             Auton : data[key]['Auton'],
                             Aim_Auton : data[key]['Aim_Auton'],
                             Hot : data[key]['Hot'],
                             Zone : data[key]['Zone'],
                             Defender : data[key]['Defender'],
                             Aim_High : data[key]['Aim_High'],
                             Truss : data[key]['Truss'],
                             Passes : data[key]['Passes'],
                             Last_Modified : data[key]['Last_Modified'],
                             Picture : data[key]['Picture'],
                             Robot_Type : data[key]['Robot_Type'],
                             Json : data[key]['Json'],
                             opr : data[key]['opr'],
                             events: data[key]['events'],
                             opr: data[key]['opr'],
                             dpr: data[key]['dpr'],
                             oprrank: data[key]['oprrank'],
                             oprpercent: data[key]['oprpercent']
                             }
                        );
                        
                        //imported[data[key]['Number']] = 1
                      //}
                var certificateList = $('#listView').data('kendoMobileListView');
                certificateList.dataSource.read();   // added line
                certificateList.refresh();  
                }
                  robotsDB.saveChanges();
                  $('#status').html('');
                  }, 300);

          }
        });
}

});

    
    matchesViewModel = kendo.observable({
        team1RedAssist: '',
        team1WhiteAssist: '',
        team1BlueAssist: '',
		match: '1',
    	cycle: 'A',
        allianceColor: '',
        comment: '',
    	team1: '',
    	team2: '',
    	team3: '',
    	venue: '',
    	team1Truss: false,
    	team1Catch: false,
    	team1HighScore: false,
    	team1LowScore: false,
    	team2Truss: false,
    	team2Catch: false,
    	team2HighScore: false,
    	team2LowScore: false,
    	team3Truss: false,
    	team3Catch: false,
    	team3HighScore: false,
    	team3LowScore: false,

        team1High: '',
        team1Low: '',
        team2High: '',
        team2Low: '',
        team3High: '',
        team3Low: '',

        // Save the robot when you press the save button
        saveMatch: function() {
            var currentCycle = $('#currentCycle').text();
			saveCycle(currentCycle);
			alert('Match Saved');
        },
        
        selectMatch: function(e) {
        },
        
        failIncrease: function(e) { 
            console.log('lowFailIncrease');
            var data = e.button.data();
            var currentCycle = $('#currentCycle').text();
			var id = data.id + '_' + currentCycle;
            var currentScore =  $('#'+id).text();
            var newScore = parseInt(currentScore) + 1;
            $('#'+data.id+'_'+currentCycle).text(newScore);
        },
        
        failDecrease: function(e) {
            console.log('lowFailDecrease');
            var data = e.button.data();
            var currentCycle = $('#currentCycle').text();
			var id = data.id + '_' + currentCycle;
            var currentScore =  $('#'+id).text();
            var newScore = parseInt(currentScore) - 1;
            if (newScore < 1) newScore = 0;
            $('#'+data.id+'_'+currentCycle).text(newScore);
        },
        
        // Create a new match entry in the viewModel
        newMatch: function(){
           console.log('newMatch');
            var db = openDatabase('Team2959', '', 'First Wave Scouting', 2 * 1024 * 1024);
            db.transaction(function (tx) {        
                var sql = 'SELECT * FROM Matches ORDER BY match DESC LIMIT 1';
                tx.executeSql(sql, [], function (tx, results) {
                    var len = results.rows.length;
                    if (len < 1) {
                    	match = 0;
                    } else {
                        var match = parseInt(results.rows.item(0).Match);
                    }
                    if (match = '') match = 0;
                    window.nextMatch = match + 1;
        			$('#match').text(window.nextMatch);
                    console.log(nextMatch);
                })
            });
                    
           matchesViewModel.set('Robot', undefined);
           $('.auto').show();
           currentCycle = 'A';
           var color = localStorage["color"];
           console.log('Color: '+color);

           $('.cycle').hide();
           window.nextCycle = 'A';
           setTimeout(function() {
               $('#allianceColor').text(color);
               matchesViewModel.set('allianceColor', localStorage['color']);
               matchesViewModel.set('cycle', 'A');
               matchesViewModel.set('match', window.nextMatch);
               console.log(window.nextMatch);
               matchesViewModel.set('venue', localStorage['venue']);
               $('#score').css('background-color', color);
        	}, 0);
        }

    
    });
    
    // Setup the initial Application
    robotsDB.getAppSettings(function(appSettingsObject) {
        viewModel.set("appSettings",appSettingsObject );
        app = new kendo.mobile.Application(document.body, {
            transition:'slide',
            platform: 'ios',
            skin: 'blackberry'
        });
        viewModel.app = app;
        viewModel.Robots.sort({ field: 'Name', dir: "asc" });
        
    });
    
});


function initPage() {
        var color = localStorage["color"];
        var venue = localStorage["venue"];
    	$('#score').css('background-color', color);
		$('#venueDisplay').html(venue);
    	$('.cycle').hide();

        $(".sub").bind( "touchend", function(e) {
        	console.log('Subtract');
            var id = e.target.id;
            id = id.replace('Sub', '');
            console.log(id);
            var sub = $('#'+id).text();
            if (sub < 1) { 
                sub = 0;
            } else {
                sub = parseInt(sub) - 1;
            }
            $('#'+id).text(sub);
            if (id == 'matchNumber') {
                changeMatch(sub);
            }

        });

        $(".add").bind( "touchend", function(e) {
        	console.log('Add');
            var id = e.target.id;
            id = id.replace('Add', '');
            var add = $('#'+id).text();
            add = parseInt(add) + 1;
            $('#'+id).text(add);
            window.currentCycle = $('#currentCycle').text();
            
            if (id == 'matchNumber') {
                changeMatch(add);
            }
        });
   
        $("#prevCycle").bind( "touchend", function() {
            console.log('Previous Cycle');
            var cycle = new Array();
            window.currentCycle = $('#currentCycle').text();
            console.log('Cycle: '+currentCycle);
            window.currentMatch = matchesViewModel.get('match');
			allianceColor = matchesViewModel.get('allianceColor');
            if (!currentCycle) currentCycle = '1';
            if (!currentMatch) currentMatch = '1';
            cycleArray[currentCycle] = [];
            if (currentCycle == '1' || currentCycle == 'A') {
                saveCycle(currentCycle);
                $('.auto').show();
                currentCycle = 'A';
                $('.cycle').hide();
                window.nextCycle = 'A';
                matchesViewModel.set('cycle', nextCycle);
                $('#currentCycle').text(window.nextCycle);
            } else {
                saveCycle(currentCycle);
                matchesViewModel.set('cycle', nextCycle);
                window.nextCycle = parseInt(currentCycle) - 1;
                $('#currentCycle').text(window.nextCycle);                
            }
            window.nextCycle = parseInt($('#currentCycle').text());
            setTimeout(function(){ fillCycle(allianceColor); }, 300);
            console.log(nextCycle);

        });
    
        $("#nextCycle").bind( "touchend", function() {
            console.log('Next Cycle');
            var cycle = new Array();
            window.currentCycle = $('#currentCycle').text();
            allianceColor = matchesViewModel.get('allianceColor');
            window.currentMatch = matchesViewModel.get('match');
            if (!currentCycle) currentCycle = '1';
            if (!currentMatch) currentMatch = '1';
            cycleArray[currentCycle] = [];
            console.log('Current Cycle: '+currentCycle);
            if (currentCycle == 'A') {
                saveCycle(currentCycle);
                $('.auto').hide();
                currentCycle = 0;
                $('.cycle').show();
                window.nextCycle = parseInt(currentCycle) + 1;
                matchesViewModel.set('cycle', nextCycle);
                $('#currentCycle').text(window.nextCycle);
            } else {
                saveCycle(currentCycle);
                matchesViewModel.set('cycle', nextCycle);
                window.nextCycle = parseInt(currentCycle) + 1;
                $('#currentCycle').text(window.nextCycle);                
            }
            window.nextCycle = parseInt($('#currentCycle').text());
            setTimeout(function(){ fillCycle(allianceColor); }, 300);            
            console.log('Next Cycle: '+nextCycle);

        });
}

function saveCycle(currentCycle) {
    console.log('Saving Cycle: '+ currentCycle);
	var match				= parseInt(matchesViewModel.get('match'), 10);
    var cycle				= currentCycle;
    var allianceColor		= matchesViewModel.get('allianceColor');
    var comment			    = matchesViewModel.get('comment');
    var oneLight 			= matchesViewModel.get('oneLight');
    var twoLight 			= matchesViewModel.get('twoLight');
    var threeLight 			= matchesViewModel.get('threeLight');
    var team1High 			= matchesViewModel.get('team1High');
    var team1Low 			= matchesViewModel.get('team1Low');
    var team2High	 		= matchesViewModel.get('team2High');
    var team2Low 			= matchesViewModel.get('team2Low');
    var team3High 			= matchesViewModel.get('team3High');
    var team3Low 			= matchesViewModel.get('team3Low');
    var team1				= matchesViewModel.get('team1');
    var team2				= matchesViewModel.get('team2');
    var team3				= matchesViewModel.get('team3');
    var venue				= matchesViewModel.get('venue');
    var team1Truss 			= matchesViewModel.get('team1Truss');
    var team1Catch  		= matchesViewModel.get('team1Catch');
    var team1HighScore  	= matchesViewModel.get('team1HighScore');
    var team1LowScore  		= matchesViewModel.get('team1LowScore');
    var team2Truss 			= matchesViewModel.get('team2Truss');
    var team2Catch  		= matchesViewModel.get('team2Catch');
    var team2HighScore  	= matchesViewModel.get('team2HighScore');
    var team2LowScore  		= matchesViewModel.get('team2LowScore');
    var team3Truss 			= matchesViewModel.get('team3Truss');
    var team3Catch  		= matchesViewModel.get('team3Catch');
    var team3HighScore  	= matchesViewModel.get('team3HighScore');
    var team3LowScore  		= matchesViewModel.get('team3LowScore');             

    console.log('Venue:' + venue);
    // Delete the old cycle
	var db = window.openDatabase("Team2959", "", "Team2959", 200000);
    db.transaction(function (tx) {
        var sql = "DELETE FROM Matches where Match ='"+ match +"' AND Cycle = '"+ cycle +"' AND allianceColor = '"+allianceColor+"'";
        tx.executeSql(sql);
        console.log(sql);
    });

    setTimeout(function(){
        match = Math.floor(match);
        console.log('Adding Match: '+match);
        console.log('Saving Cycle as:' + cycle);
        robotsDB.Matches.add({
            Match: match,
            Cycle: cycle,
            allianceColor: allianceColor,
            comment: comment,
            Venue: venue,
            Team1: team1,
            Team2: team2,
            Team3: team3,
            oneLight: oneLight,
            twoLight: twoLight,
            threeLight: threeLight,
            team1High : team1High,
            team1Low : team1Low,
            team2High : team2High,
            team2Low : team2Low,
            team3High : team3High,
            team3Low : team3Low,
            team1Truss: team1Truss,
            team1Catch: team1Catch,
            team1HighScore: team1HighScore,
            team1LowScore: team1LowScore,
            team2Truss: team2Truss,
            team2Catch: team2Catch,
            team2HighScore: team2HighScore,
            team2LowScore: team2LowScore,
            team3Truss: team3Truss,
            team3Catch: team3Catch,
            team3HighScore: team3HighScore,
            team3LowScore: team3LowScore
        });
        robotsDB.saveChanges();
    }, 300);
}

function populateMatchList() {
    console.log('populateList');
	var db = window.openDatabase("Team2959", "", "Team2959", 200000);
    db.transaction(function (tx) {
        var sql = "SELECT * FROM Matches WHERE Cycle='A'";
		console.log(sql);
        tx.executeSql(sql, [], function(tx, results) {
			console.log('Success');
            if (results.rows) {
                var length = results.rows.length;
            }
            console.log(length);
            var html = '';
            for (var i = 0; i < length; i++) {
                console.log(results.rows.item(i));
                var out = results.rows.item(i);
                var list = "<li style='color: #fff; background-color: "+out.allianceColor+"'><a href='#MatchEditor' data-match='"+out.Match+"' data-color='"+out.allianceColor+"' class='match' onClick2='javascript: changeMatch(\""+out.Match+"\", \""+out.allianceColor+"\");'><b>Match:</b> "+out.Match+" Teams: "+out.Team1+", "+out.Team2+", "+out.Team3+"</a></li>";
	            var html = html+list;
            }
            $('#matchListView').html('<ul id="matches" data-style="inset">'+html+'</ul>');
            $('#matches').kendoMobileListView();
        })
    });
    setTimeout(function() {
        $(".match").bind( "touchend", function(e) {
            var match = e.target.attributes['data-match'].value;
            var color = e.target.attributes['data-color'].value;
            changeMatch(match, color);
        });
	}, 300);
}


function changeMatch(match, allianceColor) {
    //if (!allianceColor) var allianceColor=localStorage.['color'];
    console.log('Changing Match to: '+match);
    window.currentMatch = match;
    $('.auto').show();
    currentCycle = 'A';
    var color = localStorage["color"];
    $('.cycle').hide();
    window.nextCycle = 'A';
    matchesViewModel.set('match', match);
    matchesViewModel.set('cycle', nextCycle);
    matchesViewModel.set('allianceColor', color);
    $('#currentCycle').text(window.nextCycle);
    fillCycle(allianceColor);
    $('#score').css('background-color', allianceColor);
	$('#color').text(color);
}

function fillCycle(allianceColor) {
    console.log('Filling in Cycle: '+window.nextCycle+' Match:'+window.currentMatch+' Color: '+allianceColor);
	var db = window.openDatabase("Team2959", "", "Team2959", 200000);
    db.transaction(function (tx) {
        var color = localStorage["color"];
        if (isNaN(window.nextCycle)) window.nextCycle = 'A';
        var sql = "SELECT * FROM Matches WHERE Match='"+ window.currentMatch +"' AND Cycle='"+ window.nextCycle +"' AND allianceColor='"+allianceColor+"'";
		console.log(sql);
        tx.executeSql(sql, [], function(tx, results) {
			console.log('Success');
            if (results.rows) {
                var length = results.rows.length;
            }
            if (length == 0) {
                console.log('Cycle does not exists, emptying fields...');
                matchesViewModel.set('allianceColor', localStorage['color']);
                var team1 = matchesViewModel.get('team1');
                var team2 = matchesViewModel.get('team2');
                var team3 = matchesViewModel.get('team3');
                $('#team1Display').text(team1);
                $('#team2Display').text(team2);
                $('#team3Display').text(team3);
                if (window.nextCycle == 'A') {
    				matchesViewModel.set('team1', '');
                    matchesViewModel.set('team2', '');
                    matchesViewModel.set('team3', '');
                }
                matchesViewMode.set('oneLight', false);
                matchesViewMode.set('twoLight', false);
                matchesViewMode.set('threeLight', false);
                matchesViewModel.set('team1High', false);
                matchesViewModel.set('team1Low', false);
                matchesViewModel.set('team2High', false);
                matchesViewModel.set('team2Low', false);
                matchesViewModel.set('team3High', false);
                matchesViewModel.set('team3Low', false);
                // Team 1 Checkboxes                    
                matchesViewModel.set('team1Truss', false);
                matchesViewModel.set('team1Catch', false);
                matchesViewModel.set('team1HighScore', false);
                matchesViewModel.set('team1LowScore', false);
                // Team 2 Checkboxes                    
                matchesViewModel.set('team2Truss', false);
                matchesViewModel.set('team2Catch', false);
                matchesViewModel.set('team2HighScore', false);
                matchesViewModel.set('team2LowScore', false);
                // Team 3 Checkboxes                    
                matchesViewModel.set('team3Truss', false);
                matchesViewModel.set('team3Catch', false);
                matchesViewModel.set('team3HighScore', false);
                matchesViewModel.set('team3LowScore', false);
            }
            for (var i = 0; i < length; i++) {
                    console.log(results.rows.item(i));
                	var matchData = new Array();
                	matchData[0] = results.rows.item(i);
                	console.log(matchData[0]);
                    matchesViewModel.set('team1', matchData[0].Team1);
                    matchesViewModel.set('team2', matchData[0].Team2);
                    matchesViewModel.set('team3', matchData[0].Team3);
                    matchesViewModel.set('venue', matchData[0].Venue);
                	matchesViewModel.set('allianceColor', matchData[0].allianceColor);
                
                	// Autonomous Cycle
                	if (matchData[0].team1High == 'true') { isChecked = true; } else { isChecked = false; }
                    matchesViewModel.set('team1High', isChecked);
                	if (matchData[0].team1Low == 'true') { isChecked = true; } else { isChecked = false; }
                    matchesViewModel.set('team1Low', isChecked);
					if (matchData[0].team2High == 'true') { isChecked = true; } else { isChecked = false; }
                    matchesViewModel.set('team2High', isChecked);
                	if (matchData[0].team2Low == 'true') { isChecked = true; } else { isChecked = false; }
                    matchesViewModel.set('team2Low', isChecked);
                	if (matchData[0].team3High == 'true') { isChecked = true; } else { isChecked = false; }
                    matchesViewModel.set('team3High', isChecked);
                	if (matchData[0].team3Low == 'true') { isChecked = true; } else { isChecked = false; }
                    matchesViewModel.set('team3Low', isChecked);
                
                    // Team 1 Checkboxes        
                	if (matchData[0].oneLight == 'true') { isChecked = true; } else { isChecked = false; }
                    matchesViewModel.set('oneLight', isChecked);
                	if (matchData[0].twoLight == 'true') { isChecked = true; } else { isChecked = false; }
                    matchesViewModel.set('twoLight', isChecked);
                	if (matchData[0].oneLight == 'true') { isChecked = true; } else { isChecked = false; }
                    matchesViewModel.set('twoLight', isChecked);
                	if (matchData[0].threeLight == 'true') { isChecked = true; } else { isChecked = false; }
                    matchesViewModel.set('threeLight', isChecked);	
                
                    if (matchData[0].team1Truss == 'true') { isChecked = true; } else { isChecked = false; }
                    matchesViewModel.set('team1Truss', isChecked);
                    if (matchData[0].team1Catch == 'true') { isChecked = true; } else { isChecked = false; }
                    matchesViewModel.set('team1Catch', isChecked);
                    if (matchData[0].team1HighScore == 'true') { isChecked = true; } else { isChecked = false; }
                    matchesViewModel.set('team1HighScore', isChecked);
                    if (matchData[0].team1LowScore == 'true') { isChecked = true; } else { isChecked = false; }
                    matchesViewModel.set('team1LowScore', isChecked);
                    // Team 2 Checkboxes                    
                    if (matchData[0].team2Truss == 'true') { isChecked = true; } else { isChecked = false; }
                    matchesViewModel.set('team2Truss', isChecked);
                    if (matchData[0].team2Catch == 'true') { isChecked = true; } else { isChecked = false; }
                    matchesViewModel.set('team2Catch', isChecked);
                    if (matchData[0].team2HighScore == 'true') { isChecked = true; } else { isChecked = false; }
                    matchesViewModel.set('team2HighScore', isChecked);
                    if (matchData[0].team2LowScore == 'true') { isChecked = true; } else { isChecked = false; }
                    matchesViewModel.set('team2LowScore', isChecked);
                    // Team 3 Checkboxes                    
                    if (matchData[0].team3Truss == 'true') { isChecked = true; } else { isChecked = false; }
                    matchesViewModel.set('team3Truss', isChecked);
                    if (matchData[0].team3Catch == 'true') { isChecked = true; } else { isChecked = false; }
                    matchesViewModel.set('team3Catch', isChecked);
                    if (matchData[0].team3HighScore == 'true') { isChecked = true; } else { isChecked = false; }
                    matchesViewModel.set('team3HighScore', isChecked);
                    if (matchData[0].team3LowScore == 'true') { isChecked = true; } else { isChecked = false; }
                    matchesViewModel.set('team3LowScore', isChecked);
            }
    	}, function(e) {
    	}, function() {
        	console.log('Transaction Success');
    	}
        )
    }); 
}

function strstr (haystack, needle, bool) {
  // From: http://phpjs.org/functions
  // +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // +   bugfixed by: Onno Marsman
  // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // *     example 1: strstr('Kevin van Zonneveld', 'van');
  // *     returns 1: 'van Zonneveld'
  // *     example 2: strstr('Kevin van Zonneveld', 'van', true);
  // *     returns 2: 'Kevin '
  // *     example 3: strstr('name@example.com', '@');
  // *     returns 3: '@example.com'
  // *     example 4: strstr('name@example.com', '@', true);
  // *     returns 4: 'name'
  var pos = 0;

  haystack += '';
  pos = haystack.indexOf(needle);
  if (pos == -1) {
    return false;
  } else {
    if (bool) {
      return haystack.substr(0, pos);
    } else {
      return haystack.slice(pos);
    }
  }
}

// Sync the Matches
function syncAll() {
    		var serverIp = localStorage['serverIp'];
            console.log('Starting Sync with server '+serverIp);
            var db = openDatabase('Team2959', '', 'First Wave Scouting', 2 * 1024 * 1024);
            db.transaction(function (tx) {
                var sql = 'SELECT * FROM Matches';
                console.log(sql);
                tx.executeSql(sql, [], function (tx, results) {
                    var len = results.rows.length, i;
                    for (i = 0; i < len; i++) {
                        console.log(JSON.stringify(results.rows.item(i)));
                    }
                });
                
                var sql = 'SELECT * FROM Matches';
                console.log(sql);
                tx.executeSql(sql, [], function (tx, results) {
                    var len = results.rows.length, i;
                    var imported = new Array();
                    console.log(len);
                    if (len == 0) {
                          console.log('Length is 0');

                    }
                    for (i = 0; i < len; i++) {
                        var output = JSON.stringify(results.rows.item(i));
        				var output = results.rows.item(i);
                        var imported = new Array();
                        $.ajax({
                          type: "POST",
                          url: 'http://'+serverIp+'/syncAll.php?update=1',
                          data: output,
                          success: function(data) {
                              }
                        });
                    }
                       $.ajax({
                          type: "POST",
                          url: 'http://'+serverIp+'/syncAll.php',
                          data: output,
                          success: function(data) {
                              console.log('Success');
                              //if (imported[data[key]['Number']] != 1) {
                              console.log('Deleting old robots');
                              data = JSON.parse(data);
                                db.transaction(function (tx) {
                                    var sql = "DELETE FROM Matches";
                                    tx.executeSql(sql);
                                });
                              setTimeout(function() {
                              for (var key in data) {
                                
                                  console.log(imported);
                                        console.log('Adding Match: '+data[key]['Match']);
        								robotsDB.Matches.add(
                                    		 { Match 		: data[key]['Match'], 
                                             Cycle 			: data[key]['Cycle'],
                                             allianceColor 	: data[key]['allianceColor'],
                                             comment 		: data[key]['comment'],
                                             Venue 			: data[key]['Venue'],
                                             Team1 			: data[key]['Team1'],
                                             Team2 			: data[key]['Team2'],
                                             Team3 			: data[key]['Team3'],
                                             oneLight 		: data[key]['oneLight'],
                                             twoLight	 	: data[key]['twoLight'],
                                             threeLight 	: data[key]['threeLight'],
                                             team1Truss 	: data[key]['team1Truss'],
                                             team1Catch 	: data[key]['team1Catch'],
                                             team1HighScore : data[key]['team1HighScore'],
                                             team1LowScore 	: data[key]['team1LowScore'],
                                             team2Truss 	: data[key]['team2Truss'],
                                             team2Catch 	: data[key]['team2Catch'],
                                             team3High 		: data[key]['team3High'],
                                             team2HighScore : data[key]['team2HighScore'],
                                             team2LowScore 	: data[key]['team2LowScore'],
                                             team3Truss 	: data[key]['team3Truss'],
                                             team3Catch		: data[key]['team3Catch'],
                                             team3LowScore	: data[key]['team3LowScore'],
                                             team3HighScore	: data[key]['team3HighScore'],
                                             team1High		: data[key]['team1High'],
                                             team1Low		: data[key]['team1Low'],
                                             team2High		: data[key]['team2High'],
                                             team2Low		: data[key]['team2Low']
                                             });
                                      }
                                  robotsDB.saveChanges();
                                  }, 300);

                          }
                        });
                });
            });
}


    $(document).delegate("#sortby", "change", function () {
        var f = $("#sortby").val();
        if (f != "")
            robotsDB.Robots.asKendoDataSource().sort({ field: f, dir: "asc" });
        else
            robotsDB.Robots.asKendoDataSource().sort({});
    });