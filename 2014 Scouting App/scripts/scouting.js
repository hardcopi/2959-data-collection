var Robot = $data.define("Robot", {
    Name: String,
    Number: { type: "int", key: true },
    startDate: String,
    city: String,
    state: String,
    comment: String,
    scout: String,
    Robot_Type: String,
    Drive_Train: String,
    Auton: String,
    Aim_Auton: String,
    Hot: String,
    Zone: String,
    Defender: String,
    Aim_High: String,
    Truss: String,
    Passes: String,
    Last_Modified: Date,
    Picture: { type: "blob" },
    Category_Id: Number,
    Json: { type: "blob" },
    opr: String,
    dpr: String,
    oprrank: String,
    oprpercent: String,
    events: Number
});

var RobotsDB = $data.EntityContext.extend('RobotsDB', {
    Robots: 	 { type: $data.EntitySet, elementType: Robot },
    AppSettings: { type: $data.EntitySet, elementType: AppSettings },
    Matches: 	 { type: $data.EntitySet, elementType: Match },
    
    getAppSettings: function(appSettingReady) {
        var that = this;
        function returnEntity(entity) {
            appSettingReady(entity.asKendoObservable({autoSave: true}));
        }
        
        function createAppSettings() {
            AppSettings.save({Id: 1, ShowNewItemInList: true}).then(returnEntity);
        };
        
        return that.AppSettings
        .single("it.Id == 1")
        .fail(createAppSettings)
        .then(returnEntity)
    }
});

$("#orderByDropDownList").kendoDropDownList();

function initRobotList() {
    console.log('initRobotList');
    var db = window.openDatabase("Team2959", "", "Team2959", 200000);
      db.transaction(function (tx) {
        var query = "SELECT * FROM Robots ORDER BY Number";
        tx.executeSql(query, [], function(tx, rs){
          var html = '';
          for(var i=0; i<rs.rows.length; i++) {
            var row = rs.rows.item(i);
            if (row['Last_Modified']) { var modified = "<span style='float: right;'>*</span>"; } else { var modified = ''; }
              var html = html + "<li onClick='showRobot(\""+row['Number']+"\")'>"+row['Number']+" : "+row['Name']+modified+" ("+row['city']+", "+row['state']+")</li>";
              if (i === (rs.rows.length - 1)) {
                console.log('Inserting into List');
                $('#robotListView').html(html);
                  $('#robotListView').kendoMobileListView({ style: 'inset' });
              }
          }
        });
      });
}

function showRobot(Number) {
    console.log('Changing to: '+Number);
    var db = window.openDatabase("Team2959", "", "Team2959", 200000);
    db.transaction(function (tx) {
        var query = "SELECT * FROM Robots WHERE Number="+Number;
        tx.executeSql(query, [], function(tx, rs){
			var rows = rs.rows.item(0);
            for(var index in rows) {
				var id = '#robot'+index;
                $(id).val(rows[index]);
            }
            $('#robotNameDisplay').html(rows['Name']);
            $('#robotNumberDisplay').html(rows['Number']);
            $('#smallImage').attr('src', rows['Picture']);
            app.navigate("#RobotEditor");
        });
    });
}

function saveRobot() {
    console.log('Saving Robot');
    var fields = '';
    var values = '';
    var updates = '';
    var robotFields = new Array('Name', 'Number', 'startDate', 'city', 'state', 'comment', 'scout', 'Robot_Type', 'Picture',
                         'Drive_Train', 'Auton', 'Aim_Auton', 'Hot', 'Zone', 'Defender', 'Aim_High', 'Truss', 'Passes', 'Last_Modified');
    for (var index in robotFields) {
		if (fields !== '') fields = fields + ", ";
        fields = fields + robotFields[index];
		if (values !== '') values = values + ", ";
		var value = $('#robot'+robotFields[index]).val();
        if (robotFields[index] === 'Picture') {
            var value = $('#smallImage').attr('src');
        }
        if (robotFields[index] === 'Last_Modified') {
            var value = 1;
        }
        if (value === undefined) value = '';
        values = values + "'" + value + "'";
        if (updates !== '') updates = updates + ', ';
        var updates = updates + robotFields[index] + "='"+value+"'";
    }
    
    var db = window.openDatabase("Team2959", "", "Team2959", 200000);
    db.transaction(function (tx) {
        var query = "INSERT OR IGNORE INTO Robots ("+fields+") VALUES ("+values+");";
        console.log(query);
        tx.executeSql(query, []);
        var query = "UPDATE Robots SET "+updates+" WHERE Number="+$('#robotNumber').val()+";";
        console.log(query);
        tx.executeSql(query, []);
    });    
    
    alert('Saved', $('#robotName').val());
    app.navigate("#RobotList");
}


function capturePhoto() {
    sessionStorage.removeItem('imagepath');
    // Take picture using device camera and retrieve image as base64-encoded string

    navigator.camera.getPicture(onPhotoDataSuccess, onFail, {
        quality: 49,
        saveToPhotoAlbum: true,
        sourceType: Camera.PictureSourceType.CAMERA,
        destinationType: Camera.DestinationType.FILE_URI });
}

function onPhotoDataSuccess(imageURI) { 
        // Uncomment to view the base64 encoded image data
        //console.log(imageURI);

        // Get image handle
        //
        var imgProfile = document.getElementById('smallImage');

        // Show the captured photo
        // The inline CSS rules are used to resize the image
        //
        //imgProfile.src = "data:image/jpeg;base64," + imageURI;
    	imgProfile.src = imageURI;
    	console.log(imageURI);
}

// Called if something bad happens.
// 
function onFail(message) {
    console.log('Failed because: ' + message);
    alert('Failed because: ' + message);
}

function movePic(file){ 
    window.resolveLocalFileSystemURI(file, resolveOnSuccess, resOnError); 
} 

//Callback function when the file system uri has been resolved
function resolveOnSuccess(entry){
    console.log('Moving File');s
    var d = new Date();
    var n = $('#robotNumber').val();
    //new file name
    var newFileName = n + ".jpg";
    var myFolderApp = "robotPhotos";

    window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function(fileSys) {      
    //The folder is created if doesn't exist
    fileSys.root.getDirectory( myFolderApp,
                    {create:true, exclusive: false},
                    function(directory) {
                        entry.moveTo(directory, newFileName,  successMove, resOnError);
                    },
                    resOnError);
                    },
    resOnError);
}

//Callback function when the file has been moved successfully - inserting the complete path
function successMove(entry) {
    console.log(entry);
    //I do my insert with "entry.fullPath" as for the path
}

function resOnError(error) {
    console.log('Error:'+error.code);
}


