function initSetup() {
    var color = localStorage["color"];
    var venue = localStorage["venue"];
    var serverIp = localStorage["serverIp"];
    $('#color').val(color);
    $('#venue').val(venue);
    $('#serverIp').val(serverIp);
}

function emptyDatabase() {
    var r = confirm("Are you sure you want to empty the database?\n\nThis is irreversible.");
    if (r == true) {
      	var db = window.openDatabase("Team2959", "", "Team2959", 200000);
        db.transaction(function (tx) {
            var sql = "DELETE FROM Matches";
            tx.executeSql(sql);
            var sql = "DELETE FROM Robots";
            tx.executeSql(sql);
            console.log(sql);
            alert('Databases Empty');
        });
      }
}

function saveSetup() {
    console.log('Saving Setup');
    var color = $('#color').val();
    var venue = $('#venue').val();
    var serverIp = $('#serverIp').val();
    localStorage["color"] = color;
    localStorage["venue"] = venue;
    localStorage["serverIp"] = serverIp;
    // After Sync Navigate back to the previous page
    viewModel.app.navigate("#:back");
}

function importTeamList() {
    var db = window.openDatabase("Team2959", "", "Team2959", 200000);
    var eventId = $('#venue').val();
    var serverIp = localStorage['serverIp'];
    $.getJSON( "http://" + serverIp + "/eventList.php?eventId="+eventId, function( data ) {
      $.each( data, function( key, val ) {
        console.log('Adding: ' + val['teamNickname']);
        db.transaction(function (tx) {
            var sql = "INSERT INTO Robots (`Name`, `Number`, `startDate`, `city`, `state`, `comment`, `scout`, `Robot_Type`, `Picture`, `Drive_Train`, `Auton`, `Aim_Auton`, `Hot`, `Zone`, `Defender`, `Aim_High`, `Truss`, `Passes`, `Last_Modified`)"+
                	  "VALUES ('"+val['Name']+"', '"+val['Number']+"', '"+val['startDate']+"', '"+val['city']+"', '"+val['state']+"', '"+val['comment']+"', '"+val['scout']+"', '"+val['Robot_Type']+"', '"+val['Picture']+"', '"+val['Drive_Train']+"', '"+val['Auton']+"', '"+val['Aim_Auton']+"', '"+val['Hot']+"', '"+val['Zone']+"', '"+val['Defender']+"', '"+val['Aim_High']+"', '"+val['Truss']+"',  '"+val['Passes']+"', '')";
            tx.executeSql(sql);
        });
      });
    });
}

function exportTeamList() {
    var db = openDatabase('Team2959', '', 'First Wave Scouting', 2 * 1024 * 1024);
    db.transaction(function (tx) {
        var sql = 'SELECT * FROM Robots WHERE Last_Modified=1';
        tx.executeSql(sql, [], function (tx, results) {
            var len = results.rows.length, i;
            var serverIp = localStorage['serverIp'];
            if (!serverIp) {
                alert('You need to enter a server address before syncing', 'Error');
                return;
            }
            console.log('Number of Robots Found...'+len);
            console.log(results);
            for (i = 0; i < len; i++) {
                console.log(i);
                var output = results.rows.item(i);
                
                console.log('Syncing '+output.Name);
                console.log('Uploading Robot...');
                $.ajax({
                    type: "POST",
                    url: 'http://'+serverIp+'/syncRobot.php',
                    data: output,
                    success: function(data) {
                    }
                });
                if (i === (len - 1)) {
                	alert (len + ' total robots synced...', 'Success');
                    var sql = "UPDATE Robots SET Last_Modified=''";
                    tx.executeSql(sql);
                } 
            } 
    });
});
}



